import {api} from '../services';
import * as Actions from "../constants/genres_constants";

export const LoadGenres = () => {
	return (dispatch) => {
		dispatch(onLoadGenres.request());
		return onLoadGenres.fetch()
			.then(({ data }) => {
				dispatch(onLoadGenres.success(data));
			})
			.catch((error) => {
				dispatch(onLoadGenres.error(error))
			});
	}
};

const onLoadGenres = {
	request: () => ({
		type: Actions.LOAD_GENRES_REQUEST
	}),
	fetch: () => {
		return api.request.get('/genre/movie/list');
	},
	success: (payload) => {
		return {
			type: Actions.LOAD_GENRES_SUCCESS,
			payload
		}
	},
	error: (payload) => ({
		type: Actions.LOAD_GENRES_ERROR,
		errors: payload
	})
};