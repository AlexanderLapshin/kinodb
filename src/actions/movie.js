import { api } from "../services";
import * as Actions from "../constants/movie_constants";

import { LoadGenres } from "./genres";
import { LoadMovieTrailers } from "./trailers";

export const LoadMovie = (movie_id) => {
    return (dispatch) =>
        Promise.all([
            dispatch(LoadGenres()),
            dispatch(LoadMovieData(movie_id)),
            dispatch(LoadMovieTrailers(movie_id))
        ]);
};

const LoadMovieData = (movie_id) => {
    return (dispatch) => {
        dispatch(onLoadMovie.request());
        return onLoadMovie
            .fetch(movie_id)
            .then(({ data }) => {
                dispatch(onLoadMovie.success(data));
            })
            .catch((error) => {
                dispatch(onLoadMovie.error(error));
            });
    };
};

const onLoadMovie = {
    request: () => ({
        type: Actions.LOAD_MOVIE_REQUEST
    }),
    fetch: (movie_id) => {
        return api.request.get(`/movie/${movie_id}`);
    },
    success: (payload) => {
        return {
            type: Actions.LOAD_MOVIE_SUCCESS,
            payload
        };
    },
    error: (payload) => {
        return {
            type: Actions.LOAD_MOVIE_ERROR,
            errors: payload
        };
    }
};
