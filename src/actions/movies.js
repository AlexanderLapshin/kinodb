import storage from '../services/storage';
import api from '../services/api/index';
import * as Action from "../constants/movies_constants";

export const LoadMovies = (pageNumber = 1, filter = 'popular') => {
	return (dispatch) => {
		dispatch(onLoadMovies.request());
		return onLoadMovies.fetch(pageNumber, filter)
			.then(data => {
				dispatch(onLoadMovies.success(data));
			})
			.catch(error => {
				dispatch(onLoadMovies.error(error));
			});
	}
};

const onLoadMovies = {
	request: () => {
		return {
			type: Action.LOAD_MOVIES_REQUEST
		}
	},
	fetch: (pageNumber, filter) => {
		return api.request.get(`/movie/${filter}?page=${pageNumber}`);
	},
	success: (payload) => {
		return {
			type: Action.LOAD_MOVIES_SUCCESS,
			payload
		}
	},
	error: (payload) => ({
		type: Action.LOAD_MOVIES_ERROR,
		errors: payload
	})
};

export const LoadSearchMovies = (query, page = 1) => {
	return (dispatch) => {
		dispatch(onLoadSearchMovies.request(query));
		return onLoadSearchMovies.fetch(query, page)
			.then(data => {
				dispatch(onLoadSearchMovies.success(data));
			})
			.catch((error) => {
				dispatch(onLoadSearchMovies.error(error))
			});
	}
};

const onLoadSearchMovies = {
	request: (query) => ({
		type: Action.LOAD_SEARCH_MOVIES_REQUEST,
		searchText: query
	}),
	fetch: (query, page) => {
		return api.request.get(`/search/movie?query=${query}&page=${page}`);
	},
	success: (payload) => {
		return {
			type: Action.LOAD_SEARCH_MOVIES_SUCCESS,
			payload: payload
		}
	},
	error: (payload) => ({
		type: Action.LOAD_SEARCH_MOVIES_ERROR,
		errors: payload
	})
};

export const ChangeFilter = (filter = 'popular') => {
	return (dispatch) => {
		dispatch(onChangeFilter.request(filter))
	}
};

const onChangeFilter = {
	request: (filter) => {
		storage.set('filter', filter);
		return {
			type: Action.CHANGE_MOVIES_FILTER,
			payload: filter
		}
	}
};

export const ClearSearchText = () => {
	return (dispatch) => {
		dispatch(onClearSearchText.request());
	}
};

const onClearSearchText = {
	request: () => {
		return ({
			type: Action.CLEAR_SEARCH_TEXT
		})
	}
};