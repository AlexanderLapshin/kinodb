import {api} from '../services';
import * as Actions from '../constants/trailers_constants';

export const LoadMovieTrailers = (movie_id) => {
	return (dispatch) => {
		dispatch(onLoadTrailer.request());
		return onLoadTrailer.fetch(movie_id)
			.then(({ data }) => {
				dispatch(onLoadTrailer.success(data));
			})
			.catch((error) => {
				dispatch(onLoadTrailer.error(error))
			});
	}
};

const onLoadTrailer = {
	request: () => ({
		type: Actions.LOAD_TRAILERS_REQUEST
	}),
	fetch: (movie_id) => {
		return api.request.get(`/movie/${movie_id}/videos`);
	},
	success: (payload) => {
		return {
			type: Actions.LOAD_TRAILERS_SUCCESS,
			payload
		}
	},
	error: (payload) => {
		return {
			type: Actions.LOAD_TRAILERS_ERROR,
			errors: payload
		}
	}
};