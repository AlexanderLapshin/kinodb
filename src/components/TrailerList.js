import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { translate } from "react-i18next";

class TrailerList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleAll: props.match.params.trailers !== undefined ? true : false
        };
    }

    showAll = () => {
        const { history, match } = this.props;

        this.setState({
            visibleAll: !this.state.visibleAll
        });

        if (this.state.visibleAll)
            history.push(`/movie/${match.params.movie_id}`);
        else history.push(`/movie/${match.params.movie_id}/trailers`);
    };

    render() {
        const { trailers, isFetched, t } = this.props;

        let trailersArray = trailers;

        if (!this.state.visibleAll && trailersArray != undefined)
            trailersArray = trailersArray.slice(0, 1);

        if (!isFetched)
            return (
                <div className="trailers">
                    <div className="trailers-title">{t("Trailers")}</div>
                    <div className="loading-box"></div>
                </div>
            );

        return (
            <div className="trailers">
                <div className="trailers-title">
                    {t("Trailers")}
                    {trailers.length > 1 && (
                        <span
                            className={this.state.visibleAll ? "active" : ""}
                            onClick={this.showAll}
                        >
                            {t("Show all")}
                        </span>
                    )}
                </div>
                <div className="trailers-inline">
                    {trailersArray.map((trailer) => (
                        <iframe
                            key={trailer.key}
                            width="560"
                            height="315"
                            src={`https://www.youtube.com/embed/${trailer.key}`}
                            frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                        ></iframe>
                    ))}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        trailers: state.trailers.all,
        isFetched: state.trailers.isFetched
    };
};

export default translate("translations")(
    withRouter(connect(mapStateToProps)(TrailerList))
);
