import React, { Component } from 'react';
import Helmet from 'react-helmet';
import {translate} from 'react-i18next';

class Error extends Component {

	render(){

		const { t } = this.props;

		return (
			<div>
				<Helmet>
					<title>{t('Error')}</title>
				</Helmet>
				<div className="error-container">
					<div className="error-code">404</div>
					<div className="error">Not found</div>
				</div>
			</div>
		)
	}
}

export default translate('translations')(Error);