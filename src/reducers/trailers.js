import * as Actions from "../constants/trailers_constants";

const initialState = {
    all: [],
    isFetched: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Actions.LOAD_TRAILERS_REQUEST:
            return {
                ...state,
                all: [],
                isFetched: false
            };
        case Actions.LOAD_TRAILERS_SUCCESS:
            return {
                ...state,
                all: action.payload.results,
                isFetched: true
            };
        default:
            return state;
    }
};
